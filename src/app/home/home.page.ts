import { Component, OnInit } from "@angular/core";
import { ProductsService } from "../services/products.service";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage implements OnInit {
  products: any[];
  pageNumber: any;
  spinner: boolean;
  constructor(private productsService: ProductsService) {
    this.pageNumber = 0;
    this.spinner = true;
  }

  ngOnInit() {
    /** Solving CROS Error */
    // this.productsService.products(24, this.pageNumber).then((response: any) => {
    //   try {
    //     response.data = JSON.parse(response.data);
    //     this.spinner = false;
    //     console.log("Data", response.data);
    //     this.products = response.data.list;
    //     console.log("this.products", this.products);
    //   } catch (e) {
    //     console.error("JSON parsing error");
    //   }
    // });
    this.productsService
      .products(24, this.pageNumber)
      .subscribe((data: any) => {
        this.spinner = false;
        console.log("Data", data);
        this.products = data.list;
      });
  }

  loadData(event) {
    ++this.pageNumber;
    /** Solving CROS Error */
    // this.productsService.products(24, this.pageNumber).then((response: any) => {
    //   try {
    //     response.data = JSON.parse(response.data);
    //     this.spinner = false;
    //     console.log("Data", response.data);
    //     this.products = this.products.concat(response.data.list);
    //     event.target.complete();
    //   } catch (e) {
    //     console.error("JSON parsing error");
    //   }
    // });

    this.productsService
      .products(24, this.pageNumber)
      .subscribe((data: any) => {
        this.products = this.products.concat(data.list);
        event.target.complete();
      });
  }
}
