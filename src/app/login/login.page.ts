import { Component } from "@angular/core";
import { Facebook } from "@ionic-native/facebook/ngx";
import { GooglePlus } from "@ionic-native/google-plus/ngx";

import { Router } from "@angular/router";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { LoadingController } from "@ionic/angular";
import { Platform } from "@ionic/angular";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage {
  FB_APP_ID: number = 1537898186387392;

  constructor(
    private fb: Facebook,
    private googlePlus: GooglePlus,
    private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    private router: Router,
    private platform: Platform,
    public alertController: AlertController
  ) {}

  async doFbLogin() {
    let permissions = new Array<string>();
    permissions = ["public_profile", "email"];
    this.fb.login(permissions).then(
      response => {
        let userId = response.authResponse.userID;
        this.fb.api("/me?fields=name,email", permissions).then(user => {
          user.picture =
            "https://graph.facebook.com/" + userId + "/picture?type=large";
          this.nativeStorage
            .setItem("facebook_user", {
              name: user.name,
              email: user.email,
              picture: user.picture
            })
            .then(
              () => {
                this.router.navigate(["/home"]);
              },
              error => {
                console.log(error);
              }
            );
        });
      },
      error => {
        console.log("erros", error);
      }
    );
  }

  async doGoogleLogin() {
    this.googlePlus
      .login({
        scopes: "", // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        webClientId:
          "829931421549-jj93rkgn52eg7tpdt69l3i74b17cih9b.apps.googleusercontent.com", // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        offline: true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      })
      .then(
        user => {
          this.nativeStorage
            .setItem("google_user", {
              name: user.displayName,
              email: user.email,
              picture: user.imageUrl
            })
            .then(
              () => {
                this.router.navigate(["/home"]);
              },
              error => {
                console.log(error);
              }
            );
        },
        err => {
          console.log(err);
        }
      );
  }

  async doTwLogin() {
    /* Witting For Twitter Account Developer And Then Do Implementaion*/
    // this.tw.login().then(
    //   res => {
    //     // Get user data
    //     // There is a bug which fires the success event in the error event.
    //     // The issue is reported in https://github.com/chroa/twitter-connect-plugin/issues/23
    //     this.tw.showUser().then(
    //       user => {
    //         console.log(user);
    //       },
    //       err => {
    //         console.log(err);
    //         // default twitter image is too small https://developer.twitter.com/en/docs/accounts-and-users/user-profile-images-and-banners
    //         var profile_image = err.profile_image_url_https.replace(
    //           "_normal",
    //           ""
    //         );
    //         this.nativeStorage
    //           .setItem("twitter_user", {
    //             name: err.name,
    //             userName: err.screen_name,
    //             followers: err.followers_count,
    //             picture: profile_image
    //           })
    //           .then(
    //             () => {
    //               this.router.navigate(["/user"]);
    //             },
    //             error => {
    //               console.log(error);
    //             }
    //           );
    //       }
    //     );
    //   },
    //   err => {
    //   }
    // );
  }
}
