import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { HTTP } from "@ionic-native/http/ngx";

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  api: string;
  httpOptions: any;
  constructor(private http: HttpClient, private httpNative: HTTP) {
    this.api = "http://tindiostag.tindio.com/api/";
    this.httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      })
    };
  }

  products(itemsPerPage: any, pageNumber: any) {
    /** Solving CROS Error */
    // return this.httpNative.get(
    //   this.api + "home?items_per_page=" + itemsPerPage + "&page=" + pageNumber,
    //   {},
    //   {}
    // );
    return this.http.get(
      this.api + "home?items_per_page=" + itemsPerPage + "&page=" + pageNumber,
      this.httpOptions
    );
  }
}
